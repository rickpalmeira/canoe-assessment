<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\FundController;
use App\Http\Controllers\ManagerController;
use Illuminate\Support\Facades\Route;

Route::get('/welcome', function () {
    return response()->json('Hello World!');
});

Route::controller(FundController::class)->group(function () {
    Route::get('/funds', [FundController::class, 'index']);
    Route::post('/funds', [FundController::class, 'store']);
    Route::get('/funds/{fund}', [FundController::class, 'show']);
    Route::put('/funds/{fund}', [FundController::class, 'update']);
});

Route::controller(CompanyController::class)->group(function () {
    Route::get('/companies', [CompanyController::class, 'index']);
    Route::post('/companies', [CompanyController::class, 'store']);
    Route::get('/companies/{company}', [CompanyController::class, 'show']);
    Route::put('/companies/{company}', [CompanyController::class, 'update']);
});

Route::controller(ManagerController::class)->group(function () {
    Route::get('/managers', [ManagerController::class, 'index']);
    Route::post('/managers', [ManagerController::class, 'store']);
    Route::get('/managers/{manager}', [ManagerController::class, 'show']);
    Route::put('/managers/{manager}', [ManagerController::class, 'update']);
});
