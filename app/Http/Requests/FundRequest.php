<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $date = new \DateTime('now');

        return [
            'name' => ['required'],
            'year' => ['bail', 'required', 'integer', 'max:' . $date->format('Y')],
            'manager_id' => ['bail', 'required', 'integer', 'exists:managers,id'],
            'aliases' => ['array'],
            'companies' => ['array'],
            'companies.*' => ['integer', 'exists:companies,id'],
        ];
    }
}
