<?php

namespace App\Http\Controllers;

use App\Events\DuplicateFundWarning;
use App\Http\Requests\FundRequest;
use App\Models\Fund;
use App\Models\Manager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Builder;

class FundController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $results = Fund::on();

        if (request()->has('manager')) {
            $results = Fund::whereHas('manager', function ($query) {
                $query->where('name', 'like', '%' . request('manager') . '%');
            });
        }

        if (request()->has('name')) {
            $results = $results->where('name', 'like', '%' . request('name') . '%');
        }

        if (request()->has('year')) {
            $results = $results->where('year', request('year'));
        }

        return response()->json($results->with('manager', 'aliases', 'companies')->get());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $ruleset = new FundRequest();
        $validator = Validator::make($request->all(), $ruleset->rules());

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $fund = Fund::create([
            'name' => $request->name,
            'year' => $request->year,
            'manager_id' => $request->manager_id,
        ]);

        foreach ($request->aliases ?? [] as $alias) {
            $fund->aliases()->create(['name' => $alias]);
        }

        foreach ($request->companies ?? [] as $companyId) {
            $fund->companies()->attach($companyId);
        }

        // check for duplicates
        $duplicates = Fund::where('manager_id', $fund->manager_id)
            ->where('name', $fund->name)
            ->orWhereHas('aliases', function (Builder $query) use ($fund) {
                $query->whereIn('name', $fund->aliases->pluck('name'));
            })
            ->count();
        if ($duplicates > 0) {
            new DuplicateFundWarning($fund);
        }

        return response()->json(
            Fund::with('manager', 'aliases', 'companies')->find($fund->id),
            201
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(Fund $fund)
    {
        return response()->json(
            Fund::with('manager', 'aliases', 'companies')->find($fund->id)
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Fund $fund)
    {
        $ruleset = new FundRequest();
        $validator = Validator::make($request->all(), $ruleset->rules());

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $fund->aliases()->delete();
        $fund->companies()->detach();
        $fund->update($request->all());

        foreach ($request->aliases ?? [] as $alias) {
            $fund->aliases()->create(['name' => $alias]);
        }

        foreach ($request->companies ?? [] as $companyId) {
            $fund->companies()->attach($companyId);
        }

        return response()->json(Fund::with('manager', 'aliases', 'companies')->find($fund->id));
    }
}
