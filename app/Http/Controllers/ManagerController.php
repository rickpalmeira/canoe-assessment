<?php

namespace App\Http\Controllers;

use App\Models\Manager;
use Illuminate\Http\Request;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json(Manager::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return response()->json(Manager::create($request->all()), 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Manager $manager)
    {
        return response()->json($manager);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Manager $manager)
    {
        return response()->json($manager->update($request->all()));
    }
}
