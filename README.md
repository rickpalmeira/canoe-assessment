## About This Assessment

This is my implementation of the take-home test for Remotely regarding an opportunity to join the team at Canoe.

Since the job opening states that I will be working mostly with Laravel + Postgres, I've decided to do the test using that same stack. Using the recently released Laravel 11 with Sail and Artisan to bootstrap most of the boilerplate, this project follows the approach of "Convention over configuration" to ensure it follows the most commonly used standards for this kind of API.

Data is being stored and managed according to the following layout:

![ER Diagram](public/ER%20Diagram.jpg)

## Running the Project

The only requirement for launching this application is having Docker installed and port 80 available.

After cloning this repository, please follow these steps:

1. Copy the `.env.example` file to `.env`:

```$ cp .env.example .env```

2. Start the Docker containers:

```$ ./vendor/bin/sail up```

3. Run database migrations:

```$ ./vendor/bin/sail artisan migrate```

After completing these steps, the following endpoints will become available locally:

- **GET and POST** `/api/companies`
- **GET and PUT** `/api/companies/{company}`
- **GET and POST** `/api/funds`
- **GET and PUT** `/api/funds/{fund}`
- **GET and POST** `/api/managers`
- **GET and PUT** `/api/managers/{manager}`

The documentation regarding supported and required fields can be seen [here](https://gitlab.com/rickpalmeira/canoe-assessment/-/blob/main/public/openapi.yml).
